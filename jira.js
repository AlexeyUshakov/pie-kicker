const axios = require('axios')

const SETTINGS = require('./settings')

const Jira = {
  home: SETTINGS.JIRA.home,
  headers: SETTINGS.JIRA.headers,
  search: function (jql) {
    return new Promise((resolve) => {
      console.log(`Jira.search( jql: ${jql} )`)

      function get(startAt = 0, tasks = new Array()) {
        console.log(` * get startAt:${startAt} tasks.length:${tasks.length}`)
        let url = `${Jira.home}/rest/api/latest/search?jql=${jql}&maxResults=1000&startAt=${startAt}`
        return axios.get(url, Jira.headers)
          .then(xhr => {
            console.log(' ** response')
            console.log(` ** data.issues.length: ${xhr.data.issues.length} `)
            console.log(` ** data.startAt: ${xhr.data.startAt}`)
            console.log(` ** data.maxResults: ${xhr.data.maxResults} `)
            console.log(` ** data.total: ${xhr.data.total} `)

            tasks = [...tasks, ...xhr.data.issues]

            if (tasks.length < xhr.data.total) {
              startAt += xhr.data.maxResults
              get(startAt, tasks)
            } else {
              console.log('\x1b[33m%s\x1b[0m', `Jira.search( done with ${tasks.length} tasks`)
              resolve(tasks)
            }
          })
          .catch((e) => {
            console.log(' ** url', url)
            console.log(' ** e.hostname:', e.hostname)
            console.log(' ** e.code', e.code)
            console.log(' ** e', e.toJSON())
          })
      }
      get()
    })
  },
}

module.exports = Jira
