const fs = require('fs-extra')

const Jira = require('./jira')
const SETTINGS = require('./settings')

function validateDate(log) {
    return (new Date(SETTINGS.START_DATE) <= new Date(log.started) 
        && new Date(log.started)  <= new Date(SETTINGS.END_DATE)) ? true : false;
}

function getUser(log) {
    const user = SETTINGS.USER_KEYS.find(USER_KEY => log.author && log.author.key === USER_KEY && log.started)
    return user
}

function nully(d) {
    return d < 10 ? '0' + d : d
}


function hasWork(task) {
    return (task.fields.timespent 
        && task.fields.worklog 
        && task.fields.worklog.worklogs 
        && task.fields.worklog.worklogs.length) ? true : false
}

function filterTasks(tasks) {
    console.log(`* tasks.length ${tasks.length}`)
    let filtered = new Array()
    tasks.forEach(task => {
        if (hasWork(task)) {
            task.fields.worklog.worklogs.forEach((log) => {
                let user = getUser(log)
                if (user && validateDate(log)) {
                    log.key = task.key
                    log.status = task.fields.status.name
                    
                    if (task.fields.assignee) {
                        log.assigned = task.fields.assignee.displayName
                    }
                    
                    if (typeof filtered[user] === 'undefined') {
                        filtered[user] = new Array()
                    }

                    filtered[user].push(log)
                }
            });
        }
    });
    return filtered
}

function countLogs(logs) {
    let period = {
        period: `${SETTINGS.START_DATE} - ${SETTINGS.END_DATE}`,
        projects: SETTINGS.PROJECTS_KEYS,
        users: SETTINGS.USER_KEYS.join(', '),
        loggedHours: 0,
        numberOfTasks: 0,
        work: {},
    }
    for (var user in logs) {
        let userPeriod = {
            loggedHours: 0,
            numberOfTasks: 0,
            days: {},
        }

        logs[user].forEach((l) => {
            userPeriod.loggedHours = Number(Number(Number(userPeriod.loggedHours) 
                + Number(l.timeSpentSeconds / 60 / 60)).toFixed(2))
            userPeriod.numberOfTasks++
            let D = new Date(l.started)
            let date = `${D.getFullYear()}-${nully(D.getMonth() + 1)}-${nully(D.getDate())}`

            if (!userPeriod.days[date]) {
                userPeriod.days[date] = {
                    loggedHours: 0,
                    numberOfTasks: 0,
                    logs: [],
                };
            }
            
            userPeriod.days[date].logs.push({
                key: l.key,
                spentMinutes: l.timeSpentSeconds / 60,
                comment: l.comment,
                status: l.status,
                assigned: l.assigned,
            });

            userPeriod.days[date].loggedHours = Number(Number(userPeriod.days[date].loggedHours) 
                + Number(l.timeSpentSeconds / 60 / 60)).toFixed(2)
            userPeriod.days[date].numberOfTasks++
        });

        // sort by date
        let arr = Object.entries(userPeriod.days).sort()
        userPeriod.days = []
        
        arr.forEach((day, i) => {
            userPeriod.days[i] = day[1]
            userPeriod.days[i].date = day[0]
        })

        period.work[user] = userPeriod
        
        period.loggedHours += Number(userPeriod.loggedHours);
        period.numberOfTasks += userPeriod.numberOfTasks;
    }
    return period
}

function savePeriod(period) {
    fs.mkdirsSync('./output/')
    let D = new Date()
    let today = `${D.getFullYear()}-${nully(D.getMonth() + 1)}-${nully(D.getDate())}`
    fs.writeJson(`./output/from-${SETTINGS.START_DATE}-to-${SETTINGS.END_DATE}-at-${today}.json`, period)
    return
}

function withLogs(tasks) {
    let tasksWithLogs = new Array()
    tasks.forEach(t => { 
        if (t.fields.timespent) tasksWithLogs.push(t.key)
    })
    console.log('tasksWithLogs.length', tasksWithLogs.length)
    return tasksWithLogs
}

function makeReport() {
    console.log('makeReport started')
    Jira.search()
        .then(withLogs)
        .then(Jira.getTasks)
        .then(filterTasks)
        .then(countLogs)
        .then(period => {
            console.log(`From ${SETTINGS.START_DATE} to ${SETTINGS.END_DATE}
                spent ${Number(period.loggedHours).toFixed(2)} hours`)

            savePeriod(period)

            return
        })
}

// prepare and save in json file
makeReport()

/*
 * Notes:
 * Need to make extension with injection on btb jira like widget
 */