# Pie kicker
Kick in Slack channel who are in Pie
Read tasks in Jira with "Now overdue"
then filter with users from settings and send message in Slack channel with mentions

## Installation
```bash
npm install
```

## Run with 
```bash
npm start
```
or double click on start.bat

## Dependencies
JIRA app token to get tasks
Slack app url to send message

## Settings
Make .env file with
```
JIRA_TOKEN = YWxleGVxBzb2Z0b21hx29tOnhPdm5GUGxwNHFkelVWUVBGREI2Rg==
USERS = ["firstuser@softomate.com","seconduser@softomate.com","andthisuser@softomate.com"]
SLACK_CHANNEL = https://hooks.slack.com/services/TDZXNNQ/B013ZXPVUL/T8HVXfx1VlGWNxwKhgxvWsGP
GREEATING = Hey!
```

## How to create Web hook
https://api.slack.com/apps
https://slack.com/help/articles/115005265703-Create-a-bot-for-your-workspace

## How to create Jira API token
https://confluence.atlassian.com/cloud/api-tokens-938839638.html
