const axios = require('axios')
const uniq = require('lodash.uniq')
const moment = require('moment')

const Jira = require('./jira')
const SETTINGS = require('./settings')

// const pulse = 30 * 1000

const teams = SETTINGS.teams

console.log('  Pie kicker started')
console.log(`
	teams
`, teams)

function sendKick(team) {
	console.log('sendKick', team.name)
	let text = `${SETTINGS.slack.greeting}
	`
	team.inPie.forEach(user => {
		text += `<@${user}>
	`
	})

	text += `you are in Pie!`

	console.log(text)

	team.channel && axios.post(team.channel, {
    text,
  })
  .then(function (response) {
		console.log(team.name, 'Kick status - ', response.data)
  })
  .catch(function (error) {
    console.log(error.toJSON())
  })
}

function run() {
	let time = moment().format('HH:mm')
	console.log(`
	time`, time)

	Jira.search(`resolution = Unresolved AND status in ("In Dev", "In Progress", Open, Review) AND due < "${moment().format('YYYY-MM-DD')}"`)
		.then(tasks => {
			console.log('tasks.length', tasks.length)

			// filter tasks by team
			teams.forEach(team => {
				team.tasks = tasks.filter(task => {
					if (task.fields 
						&& task.fields.assignee 
						&& task.fields.assignee.emailAddress) {
							return team.users.find(user => user === task.fields.assignee.emailAddress)
					}
				})
				console.log(`
	team ${team.name}.tasks amount:`, team.tasks.length)
			})

			// filter users in pie
			teams.forEach(team => {
				team.inPie = team.users.filter(user => {
					return team.tasks.find(task => {
						if (task.fields 
							&& task.fields.assignee 
							&& task.fields.assignee.emailAddress) {
								return user === task.fields.assignee.emailAddress
							}
					})
				}).map(user => {
					let index = user.indexOf('@')
					return user.slice(0, index)
				})
					console.log(`
	team ${team.name}.inPie:`, team.inPie)
			})

			// send message for every team 
			teams.forEach(team => {
				if (team.tasks.length && team.inPie.length) {
					sendKick(team)
				}
			})
		})
}

run();
